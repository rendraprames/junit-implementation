package com.latihan.junit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class JunitApplicationTest {

	private JunitApplication junit = new JunitApplication();


	@Test
	//	@Disabled
	@DisplayName("Positive Test - Sukses Input Number")
	public void testInputSuccess() {
		var result = junit.perkalian(5, 5);
		assertEquals(25, result);
	}

	@Test
//	@Disabled
	@DisplayName("Negative Test - Failed Input Number Null")
	public void testInputNull() { junit.perkalian(null, null);
	}

	@Test
	@DisplayName("Positive Test - Sukses Input Number")
	public void testInputBagiSuccess(){
		var result = junit.pembagian(10,2);
		assertEquals(5, result);
	}

	@Test
	@DisplayName("Negative Test - Failed Input Dibagi 0")
	public void testInputBagiFaiiled(){ junit.pembagian(10,0);
	}
}