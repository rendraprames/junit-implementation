package com.latihan.junit;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Main {
	public static void main(String[] args) {
		JunitApplication junit = new JunitApplication();

//		System.out.println("------------Perkalian-----------");
//		System.out.print("Success = ");
//		//input sukses
//		junit.perkalian(5,5);
//		//input failed go to exception
//		junit.perkalian(null,null);
//		System.out.println("------------Pembagian-----------");
//		System.out.print("Success = ");
//		//input sukses
//		junit.pembagian(10,1);
//		//input failed go to exception
//		junit.pembagian(10,0);
	}
}
