package com.latihan.junit;

public class JunitApplication {

    //Method Perkalian
    public double perkalian(Integer number1, Integer number2){
        double perkalian = 0;
        try {
            perkalian = number1 * number2;
            System.out.println(perkalian);
        } catch (Exception e) {
            System.out.println("Input tidak boleh kosong");
        }
        return perkalian;
    }
    //Method Pembagian
    public double pembagian (Integer number1, Integer number2){
        double bagi = 0;
        try {
            bagi = number1 / number2;
            System.out.println(bagi);
        } catch (Exception e) {
            System.out.println("Tidak boleh dibagi 0");
        }
        return bagi;
    }
}
